Q=@

DOCS = mustard

TARGETS = $(addsuffix .pdf, $(DOCS))
DRAFT_TARGETS = $(addsuffix .draft, $(DOCS))

LATEX = pdflatex -halt-on-error -shell-escape -synctex 1
BIB = bibtex

LATEX_CLEAN = *.aux *.xml *.bcf *.bbl *.blg *-blx.bib \
		*.log *.nav *.out *.vrb *.snm *.toc \
		*.bak *.pag *.fdb_latexmk *.fls \
		*.brf *.xcp *_minted-* *.synctex.gz

DRAFT_PREFIX=\\let\\ifdraft\\iftrue

.PHONY: default
default: all-drafts

.PHONY: all-drafts
all all-drafts: $(DRAFT_TARGETS)

all-publish: $(TARGETS)

# Small trick because %.pdf: %/%.tex is not allowed
# SECONDEXPANSION means that the rule will be expanded twice.
# If `make` is invoked as `make validation.pdf`, the first expansion gives
# %.pdf: $*/$*.tex where % is matched with validation
# the second expansion gives
# validation.pdf: validation/validation.tex
.SECONDEXPANSION:
%.draft: $$*/$$*.tex $$*/**.tex
	$(eval JOB:="$(DRAFT_PREFIX) \input{$*}")
	$(Q)cd $* && $(LATEX) -jobname $* $(JOB)
	$(Q)cd $* && $(BIB) $* -min-crossrefs=3
	$(Q)cd $* && $(LATEX) -jobname $* $(JOB)
	$(Q)cd $* && $(LATEX) -jobname $* $(JOB)
	$(Q)rm -f $*.pdf && ln -s $(subst .tex,.pdf,$<) $*.pdf

%.pdf: $$*/$$*.tex $$*/**.tex
	$(Q)cd $* && $(LATEX) $*.tex
	$(Q)cd $* && $(BIB) $* -min-crossrefs=3
	$(Q)cd $* && $(LATEX) $*.tex
	$(Q)cd $* && $(LATEX) $*.tex
	$(Q)rm -f $*.pdf && ln -s $(subst .tex,.pdf,$<) $*.pdf

.PHONY: clean
clean:
	$(Q)for doc in $(DOCS) ; do \
		rm -f $$doc.pdf || true ; \
		cd $$doc ; \
		rm -f $$doc.pdf || true; \
		rm -rf $(LATEX_CLEAN) ; \
		cd .. ; \
	done

.PHONY: debug
debug:
	@echo $(DOCS)
	@echo $(TARGETS)
