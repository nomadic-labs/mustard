{


\subsubsection{Operations.}
An economic protocol defines the maximum
size of an operation $max_{op}$. Protocol operations are categorized
using the \voc{ pass} definition, called kind. If the protocol features
$n$ kinds of operations, then each category is abstract by a number
from $0$ to $n-1$. Kind $0$ operations are dedicated to the score
computation. Each kind comes with a weight, such as $0$ is the
heaviest.

\noindent $\oofkind{op}$ returns either $k$ $\operation{op}$ kind or $\bot$ in case of ill-formed.

\noindent $\ocomp{o_1}{o_2}$ states that, in a list of operation list, $\operation{o_1}$ should appears before $\operation{o_2}$.

\noindent$\osizeof{op}$ returns the size of operation in bytes.


In additions to specific to the economic protocol data, an operation
also, contents shell metadata.


\subsubsection{Blocks.}
A block $\bb$ contains, in its header, its predecessor
$\bpred$ (the block upon which it has to be appended), and the
protocol that it has been baking for $p$, its score $\fit$ and its
height $\hg$. It also contains the hash of its operations
and their number of operations kind.

Operations in a block are gathered by a list of operation lists,
denoted $\oops$. Operations are gathered by kind. The lists are
ordered by kind weights. Each list will be applied in the sequential
order and the operation lists will be applied in their kind order.
$\oopsk{k}$ denotes the $k^{th}$ operation list of $\oops$.
$\oopsk{k}$ contains kind $k$ operations. $\oopse{k}{e}$ denotes the
$i^{th}$ operation in $\oopsk{k}$.

To ensure the liveness and fairness of operation applications, an
economic protocol specifies \voc{ quotas} that states two constants by
kind $k$. $\passsize{k}$ and $\passelts{k}$ defines the maximum size
and the number of kind $k$ operations in a block.

%A \voc{ block candidate} designates a block to validate.\\*
%A \voc{ valid block} is an appended block candidate which popularity is not yet computed.\\*
%A \voc{ valid new head block candidate} is a valid block with a computed popularity.\\*

In this abstraction, we ignore the distinction between the block
header and block.



\subsection{Reading the Ledger in the Storage}

%The ledger designates the database hosts on a Tezos node.
At its most low level, the replicated data structure is
a key/value storage which is named storage in the implementation.

%This storage maps a \voc{ context} to each valid block.
%In this storage, each valid block is associated to \voc{ context}.
%From this storage, we only consider the part dedicated to \voc{
%blocktree} and \voc{ blockchains} abstracted by \voc{ contexts}.

%\noindent A \voc{ context} gathers the writing due to the application of a block and
%has access to the ledger (like a back pointer). Once a block is
%validated, its context is registered in the storage.

\noindent A \voc{ chain} or blockchain is then implicit and can be computed from its head
block context by loading its predecessor block context, and so on until the genesis
block.

\noindent A \voc{ blocktree} is the set of chain in the storage.

\noindent The protocol defines \voc{ a minimal age gap for authorized forks},
$\gap$. If the main chain height is $h$ then the chain from genesis to
$h - \gap$ is the finalized prefix and contains finalzed blocks.


\subsubsection{The score.}
The Shell and the economic protocol shares the notion of a block
score, denoted $\fit$. The declared score in a block header could be
incorrect. An economic protocol always re-computes the score of block
and the Shell always makes its verification on computed by the
economic protocol scores.
For implementation efficiency,e.g. to avoid unconsistent called to the economic protocol,
it might be the case that the Shell
makes it computation on declared scores but it will check the validity
of this computation after the economic protocol.


\subsection{Wrapping Data from and to Shell}

\subsubsection{A Shell context.}
We denote $\sctx$ a Shell context that is the low-level abstraction of
a stored context: it contains data that can be interpreted only by an
economic protocol. The interaction between an economic protocol and a
Shell only deals with the Shell context. We assume that the economic
protocol inputs valid contexts. A valid context is a context output by
a validation. In a correct Shell, a context is committed into the
ledger only after a block validation.


\subsubsection{An evaluation context.}
Each shell context $\sctx$ is associated with an economic protocol
$p$. We denoted $\ectx$ an evaluation context that is a higher
abstraction of a shell context $\sctx$. An $\ectx$ is equipped with accession and
updater functions specific to $p$ that type a $\sctx$
and manages $\sctx$ according to $p$ governance rules. Notice that the
economic protocol can only write in the part concerning its associated
block, while the prefix is only readable. Shell and evaluation context
are convertible: (i) $\eofs$ converts a shell context to an evaluation
one, and (ii) $\etos$ converts an evaluation context to a shell one.



\subsubsection{Block with protocol data.}

A block header is a generic description of a block that is
independent of the protocol. {\tt block\_header\_metadata}, denoted
$\pbh$ contains more dedicated to the protocol information: the baker,
the height, the voting period, the nonce hash, the consumed gas, the
list of deactivated delegates, and the balance updates.


\subsubsection{Validation state.}
A validation state is an internal state of the protocol that enables
it to link the different steps of validation. However, from
a validation state $\valst$, the function $\ctxof{\valst}$ returns a context
$\sctx$ that corresponds to the current validation context.
We add $\bof{valst}=\bb$ if $\valst$ is an validation state of $\bb$
application.


\subsubsection{Validation result.}
The validation result, denoted $\valres$, returns a shell context
$\sctx$, a score $\fit$, the maximum time-to-live of operations
$\maxttl$ and the last allowed fork height $\sync$. Suppose $\valres$
the validation result produced by block $\bb$ validation. $\sctx$ is
the updated shell context after $\bb$ application. $\fit$ is $\bb$
score that enables comparison with the current head of the Shell.
Operation older than $\maxttl$ cannot be injected in a successor of
$\bb$ block. Finally, $\sync$ is the height before which no fork
should be accepted.


\subsubsection{Operation receipt.}
As a block header metadata, an operation receipt is specific to
the protocol. It characterizes the application of an operation
it could empty -- {\tt No\_operation\_metadata} -- or fulfilled -- {\tt
  Operation\_metadata } $\resop{op}$ is the receipt of an
operation $\operation{op}$.

\subsubsection{Error kinds.}
The Shell and an economic protocol also communicate through
error via three kinds of error:
\begin{itemize}

\item {\tt Permanent} means that the block or operation application
 will never succeed;

\item {\tt Temporary} means that the  block or the operation
 application is currently not valid but could become valid,

\item {\tt Branch} means that on this branch that block or
 the operation will never succeed but it could be the case on a fork.
\end{itemize}

\subsection{Block and Operation Applications}\label{api}

\subsubsection{Chain as a prefix.}
We denote $\prefix{\bb}$ the tuple
$\bb,\ctx{\bb},\tstp{\bb},\bfit{\bb}$ where $\bb$ is a block,
$\ctx{\bb}$ the context after its application, $\tstp{\bb}$ its baking
timestamp and $\bfit{\bb}$ its score.

\subsubsection{Valid context.}
$\val{\ctx{\bb}}$ denotes a valid context. A valid context is a
context complying the governance rules.

\subsubsection{Chaining application.}
An economic protocol features several {\it begin apply} functions that
computes a block application pre-state corresponding to the chainning
of the block to validate upon its the predecessor or ancestor.

\noindent {\it begin application}, denoted $\begapp$, computes the chaining of an existing block $\bb$ application upon its predecessor. It inputs $\bb$ and its predecessor prefix $\prefix{\bpred}$.

\noindent {\it begin partial application}, denoted $\begpapp$, computes the chaining in a partial application of a block $\bb$ upon an ancestor block. It inputs $\bb$ and one of its ancestor prefix $\block{anc}$ with  $\sync < \bhg{\block{anc}} < \bhg{\bb} - 1$.

\noindent{\it begin construction}, denoted $\begconst$, computes a the chaining of a mocked block (or a block in construction). It inputs $\prefix{\bpred}$ the supposed predeccesor of the mocked
block and $\tau$ a mocked timestamp baking for the mocked block. It
also might input a $\pbh$.

Begin application functions output a validation state from which one
can reach the chaining context of the block to validate.

\subsubsection{Operation Application.}
{\it operation application}, denoted $\appop$, computes the application
of an opearation $\op$ in a (valid) context. This valid context is abstracted
by a validati validation $\valst$.
$\appop~\op~\valst$ returns the a validation state $\valst$, that is the update of
$\valst$ with the application of $\op$, and the operation receipt $\resop{\op}$ that
described this update.

\subsubsection{Finalize block.}
Finalize block, denoted $\finalize$, performs the application last
step on a validation state $\valst$. It verifies that the appending
of $\bb$ is performed up on an enough popular (head) chain.
$\finalize~\valst$ returns a pair of a validation result $\valres$ and a block header enriched
protocol data $\pbh$. If $\finalize~\valst= (\valres, \pbh) $ then $\valres$:
\begin{enumerate}[label=(\roman*)]
 \item outputs the context after the validation abstracted in
$\valst$,
 \item the potentially updated $\maxttl$,
 \item the updated $\sync$,
 \item $\pbh$ the validated block $\bb$ enriched with data
which semantics depends on the economic protocol.
\end{enumerate}


\subsection{Additional Functions for Validation}

To ease the formalization, we define some application functions that
gather sequential function calls as schemed in the figure~\ref{fig2}.
They fit the interactions with the validation sub-systems.

\subsubsection{Application of a list of operation lists.}
 $\appops$ is the sequential application of $\appop$ (fold left) on a
 list of operation lists $\oops$. We denote
 $\appop~\operation{o_1};\operation{o_2}~\valst$ the sequential
 application of operation $\operation{o_2}$ after applying
 $\operation{o_1}$ defined as follow:

\noindent
$ \appop~\operation{o_1}~\valst = \valstv{1},\resop{\operation{o_1}}  \land
    \appop~\operation{o_2}~\valstv{1} = \valstv{2},\resop{\operation{o_2}} \rightarrow
\appop~(\operation{o_1};\operation{o_2})~\valst =\valstv{2},[\resop{1};\resop{2} ].$

\noindent
$\appops~\oops~\valst= \valstv{out}, \resops$ where $\appops$ is the
expected fold left application of $\appop$.
Notice that we use the same notation for a flatten operation list application.

\subsubsection{Application.} $\app$ implements an existing
block application.
$\app$ inputs the block de validate $\bb$ and its predecessor state $\prefix{\bpred}$
$\app~\prefix{\bpred}~\bb$ calls sequentially:
\begin{enumerate} [label =(\roman*)]
 \item $\begapp~\prefix{\bpred}~\bb$ returns $\valstv{alloc}$ the chaining state of $\bb$ upon $\bpred$,
 \item $\appops~\oops~\valstv{alloc}$ in $\valstv{alloc}$, $\appops$ applies $\oops$ the
operations that constitutes $\bb$ and returns $\valstv{content}$ the content evaluation context
and $\resops{\oops}$ the description of the update between $\valstv{alloc}$ and $\valstv{content}$,
 \item $\finalize~\valstv{content}$ returns the validation result $\valres$.
\end{enumerate}
$\app$ returns $\valres,\resops{\oops}$

\subsubsection{Partial application.}
$\papp$ corresponds to the partial validation.
It evaluates if a block $\bb$ has been appending upon an enough popular chain.
${\bf \block{anc}}$ is a potential ancestor of $\bb$.
$\papp~\prefix{\bf{\block{anc}}}~\bb$ calls sequentially:
\begin{enumerate}[label =(\roman*)]
 \item $\begpapp~\prefix{\block{anc}}~\bb$ returns the chaining state
$\valstv{alloc}$ of $\pp$ upon its potential ancestor $\block{anc}$,
 \item $\appops~\oopsk{0}~\valstv{alloc}$ returns $\valstv{content}$ the evaluation state of
 kind $0$ operations application $\resops{\oops{0}}$,
\item $\finalize~\valstv{content}$ verifies the popularity of the chain's head up on which
$\bb$ attempts to be appended and returns $\valres$.
\end{enumerate}
$\papp~\prefix{\bf{\block{anc}}}~\bb$ returns $(\valres, \resops{\oopsk{0}}~list).$


\subsubsection{Pre-application.}
A pre-validator keeps two kinds of operation, the ones that are
currently valid, {\it known\_valid}, and the one that might become valid,
{\it pending}. At each current head change, the pre-validation allocates a
mocked block via $\begconst$ with the current main chain as a prefix
$\prefix{\bpred}$. The output validation state, $\valstv{alloc}$,
becomes its maintained validation state, denoted $\valstv{preval}$.
Abstractly, the pre-validation calls $\appop$ on each received
operation, $\operation{rcv}$, with $\valstv{preval}$ as input. In the
case of {\tt Temporary} the operation is stored in the {\it pending}.
In the case of {\tt Permanent} or {\tt Branch}, the operation is
deleted. In case of success, the operation is stored in {\it valid}
and $\valstv{preval}$ is updated. $\preapp$ inputs parameters the
$\valstv{preval}$. As output, $\preapp$ either returns ${\tt
Applied}~\valst | {\tt Branch} | {\tt Permanent} | {\tt Temporary}$, 
the operation receipt is ignored.



%% \paragraph{Atomic pre-application.}
%% The Replicated State Machine does not keep track of the mocked block
%% and the validation state. It calls the prevalidation with
%% the predecessor prefix,
%% $valid$ and $pending$ operation lists, and
%% $\operation{o}$. As a result, it deals with {\tt
%% Applied~|~Branch~|~Permanent~|~Temporary}.
%% We define the function $\preappat$ that inputs a predecessor prefix $\prefix{\bpred}$, an already validated operation list $valid$ and an operation $\operation{o}$.
%% We assume that $valid$ application and the allocation of the mocked block cannot failed. We also assume that the Replicated State Machine inputs a mocked timestamp  $\tau$.
%% $\preappat~\bpred~\ctx{\bpred}~\bhg{\bpred}~\bfit{\bpred}~\tau~valid~\operation{o}$:
%% \begin{enumerate}[label=(\roman*)]
%% \item $\begconst~\bpred~\ctx{\bpred}~\bhg{\bpred}~\bfit{\bpred}~\tau=(\pbh,\valstv{alloc})$,
%% \item
%%   \begin{itemize}
%%    \item $\appops~\valstv{alloc}~valid::\operation{o}=\valstv{preval}$ then returns {\tt Applied}
%%    \item $\appops~\valstv{alloc}~valid::\operation{o}=(err={\tt Permanent|Branch|Temporary})$ then
%% returns $err$.
%%  \end{itemize}
%% \end{enumerate}


}
