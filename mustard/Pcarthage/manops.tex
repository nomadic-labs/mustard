{

A manager operation $\op$ application evaluates according to the following rule: 


\begin{mathpar}
\inferrule
 { 0 \leq \stlim{\op} < \cstlim  \\
   \wallets{\prectx}~\source{\op} = \wl \\
   \bal{\wl} \neq \bot \\
   \ocounter{\op} = \acter{\prectx}{\source{\op}} + 1 \\
  \ttappx{\op}{\piectx{1}}{\str}{\etr} \\
  \teval{\ftrans{\piectx{1}}{\source{\op}}{\fee{\op}}}{\piectx{2}}{\estake} \\
  \fup{\psectx}{\piectx{2}}{\gcter{},\acter{}{\source{\op}}}{\incr{\gcter{\piectx{1}}},\incr{\acter{\piectx{1}}{\source{\op}}}}}
 {\appmanopcc{\op~pk}{\estake}}
\end{mathpar}

\bigskip

We define an evaluation state $\cctx{\cectx}{\str}{\etr}$ 
the triplet of an evaluation context $\cectx$, an stake entry trace $\str$
and a Tez entry trace $\etr$. 
An manager operation $\op$ issued by $pk$ in an initial evaluation state $\prcctx$ evaluates 
into an output evaluation state $\pscctxp{op}$ if:
\begin{enumerate}[label=(\roman*)]
 \item The annonced required storage is bounded by the storage limit for operation application: 
  $0 \leq \stlim{\op} < \cstlim$,
 \item The source wallet is not empty:$\wallets{\prectx}~\source{\op} = \wl \land \bal{\wl} \neq \bot $, 
 \item $\op$ is the next entry to be evaluated for $\op$ source: $\ocounter{\op} = \acter{\prectx}{\source{\op}} + 1$,
 \item $\op$ in $\prectx$ evaluates in $\psectx$ with $\pistr{op}$ as stake trace and $\pietr{op}$ as Tez entry trace,
 \item the entry counters have been incremented and
 \item the fees have been paid. 
\end{enumerate}


\begin{figure}[ht]
\begin{mathpar}

\inferrule
{\tevalc{\wmng{\prectx}{\source{\op}}{\#k}}{()}}
{\ttappc{\operation{Reveal~\#k}}{()}{()}} 
\quad (\textsc{Reveal})



\inferrule
{\chksgn{pk}{\op} \\
 \teval{\deploy{\prectx}{\ct}}{\piectx{1}}{()} \\
 \ttevalc{\scinit{\prectx}{src}{mng}{\ct}}{\str}{\etr}}
{\ttappc{\operation{Origination~\ct}}{\str}{\etr}}
\quad({\textsc{Deploy}})

\inferrule
{\chksgn{pk}{\op} \\
 \ttevalc{\ttrans{\prectx}{src}{am}{dest}}{\str}{\etr}
}
{\ttappc{\operation{Transaction~rcpt~am}}{\str}{\etr}}
\quad ({\textsc{Trans}})


% NB: Without internal operation, payer=source. To modify when adding internal ops
\inferrule
{ \chksgn{pk}{\op} \\
  \kcontract{dest} = \ccst{\kappa_{sc}} \\
  \ttevalx{\ttrans{\prectx}{\source{\op}}{am}{rcp}{\pars}}{\piectx{1}}{\pistr{1}}{\pietr{1}} \\
  \smcs{\piectx{1}}~dest = \ct \\
  \eval{\invoke{\piectx{1}}{dest}{\ep}{\pars}}{\piectx{2},topay} \\
  \ttevalc{\payst{\piectx{2}}{\payer{\ct}}{topay}}{\str}{\etr}
}
{\ttappc{\operation{Transaction~rcp~am~\pars~\ep}}{(\pistr{1},\str)}{(\pietr{1},\etr)}}
\quad({\textsc{Invoke}})


\inferrule
{\chksgn{pk}{\op} \\
  \wallets{\prectx}~{\source{\op}} = \wl \\
 \del{\wl} = \bot \\
 \tevalc{\wdel{\prectx}{\source{\op}}{d}}{\str}
}
{\ttappc{\operation{Delegation~d}}{\str}{()}}
\quad (\textsc{Del assign})


\inferrule
{\chksgn{pk}{\op} \\
 \tevalc{\wudel{\prectx}{\source{\op}}}{\str}
}
{\ttappc{\operation{Delegation~d}}{\str}{()}}
\quad (\textsc{Del unassign})


\inferrule
{\chksgn{pk}{\op} \\
 \wallets{\prectx}~{\source{\op}} = \wl \\
 \del{\wl} \neq \bot \\
 \tevalc{\wsdel{\prectx}{\source{\op}}{d}}{\str}
}
{\ttappc{\operation{Delegation~d}}{\str}{()}}
\quad (\textsc{Del switch})


\end{mathpar}

\caption{Manager Operation Evaluation}
\label{fig:manop}
\end{figure}


The figure~\ref{fig:manop} gather the rule of manager operation evaluation.\\



\noindent \textsc{Reveal} states that evaluating a manager revalation operation
lifts the evaluation of the function $\ccst{set\_mng}$ as defined
in \tsem{(Wallet manager revelation)} of section~\ref{sem:wrev}.

\noindent \textsc{Deploy} states that evaluating a smart contract deployment, Origination in
Tezos jargon, consists in (i) register the smart contract as described
in \tsem{(Smart contract deployment)} of section~\ref{sem:depl}, (ii)
followed by the crediting of the fresh smart contract balance as
described in \tsem{(Smart Contract Initial Crediting)} of
section~\ref{sem:scc}.

\noindent \textsc{Trans} states that a simple transaction, e.g. without a smart contract invocation, lifts the evaluation of the transaction transfer function as described in \tsem{(Transaction transfer)} of section~\ref{sem:otr}.
 
\noindent \textsc{Invoke} states that evaluating a smart contract invocation consists in
(i) transfering the amount from the source to the smart contract
wallet, (ii) interpreting the invocation of the smart contract at the
entrypoint with the parameters as described in \tsem{(Smart contract
invoking)} of section~\ref{sem:call} and (iii) the payer, without
internal operation the source, paying the fees for the extra storage
if any as described in \tsem{(Smart contract Initial Crediting)} of
the section~\ref{sem:scpay}.

\noindent \textsc{Del assign} states that the assignment of a delegate to an account that has no delegate lifts the evaludation of setting a delegate as described in \tsem{(Delegate designation)} of section~\ref{sem:wdel}.

\noindent \textsc{Del unassign} states that evaluating a delegate dismissing lifts a wallet 
delegate dismissing as described in \tsem{(Wallet undelegation)} of
section~\ref{sem:udel}.

\noindent \textsc{Del switch} states that the assignment of a delegate to an account that already has a delegate lifts a wallet delegate switching as described in \tsem{(Delegate switch)} of section~\ref{sem:wds}.

Except for the manager revelation, every manager operation evaluates
only if it is signed by the source of the operation manager.

}
