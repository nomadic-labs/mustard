A Delphi evaluation context $\ectx$ interprets and manages the
context to animate its submechanism [mjg "submechanism"?]: the account management, the
accounting management, incentive distribution, the stake computation
and the baking priorities and baking rights.

The application of a block requires information computed several
blocks ago and computes information for the application of block for
several future blocks. It is the case for the stake computation, the
incentive distribution, and the baking priorities and endorsement
rights.

A $\ectx$ is also parametrized by constant. As we neither deal with
the self-amendment mechanism nor protocol switching, those constants
are not part of $\ectx$ in this formalisation. We simply refer to them
as Delphi constants.

We indicate error generation but not their propagations.


\noindent Semantics of operations that manipulate the evaluation context
are given through an evalution relation and formating definitions. 
For each operation on an evaluation context $f~params$ we define a \voc{relation}
$\evalx{f~params}{\prectx}{\psectx}$ stating that $f~\prectx~params$ returns $\psectx$ through
derivation rules. If $\prectx$ is obvious from $f~params$, it is omitted in the statement
for sake of the lisibility.


In addition, each rule comes with a \voc{definition} organizing as follows:
\begin{enumerate} [label=(\roman*)]
 \item The signature of the function; 
 \item $\Descr{}$ section, provides a description of the function;
 \item $\Pre$ section, explicits the requirements on the input evaluation context $\prectx$;
 \item $\Post$ section, that specifies the output evaluation context $\psectx$;
 \item $\Raise$ section, that lists the potential exception raising and;
 \item $\Rule$ section, that specifies $\evalc{f~params~\prectx}$. 
\end{enumerate}

In an evaludation context $\ectx$, $\cb{\ectx}$ is the currently
applied block and $\cbk{\ectx}=\bbk{\ectx}$ its baker.

An evaluation context of type $\typeof{\ectx}$ is composed of several
sub-contexts. Sub-contexts are functional maps as defined in
section~\ref{logic}. This section presents the different sub-context
of an evaluation context and gives the semantics of their
manipulations.


\subsection{Account Context} \label{ss:acc}
\input{Pcarthage/accounting}

\subsection{Wallet Context} \label{ss:w}
\input{Pcarthage/wallets}


\subsection{Incentive Distribution and Freezing} \label{ss:ins}

\paragraph{Minted and burning Tez.}
For the formalization purposes, we add two counters of type
$\typeof{\ectx} \rightarrow N$ in the evaluation context that accounts
the number of Tez that are minted and burnt in a block. 
As $\ccst{\CI}$, they have been defined for formalization purposes. 

As these counters concerned incentives, we define two maps of cycle to minted
and burnt Tez of type
$\typeof{\ectx} \rightarrow \typeof{\ccst{cycle}} \rightarrow N$ that
works with frozen accounts and punishment, for the formalization purposes.

\begin{itemize}
 \item \voc{Tez creation}: $\ctez{\ectx}$ is the amount of Tez created by $\cb{\ectx}$. 
$\ctezc{\ectx}{C}$ is the amount of Tez created in cycle $C$ known in $\ectx$. 
 \item \voc{Tez burning}: $\btez{\ectx}$ designates the Tez to burn by
$\cb{ectx}$. Tez are burnt either for the storage cost of smart
contract origination or called. 
$\btezc{\ectx}{C}$ is the amount of Tez burned in cycle $C$ known in $\ectx$.
\end{itemize}


\paragraph{Cumulative incentives by cycle.}
$\ccst{\CF}$ of type $\typeof{\ectx} \rightarrow \typeof{cycle} \rightarrow \typeof{\addr} \typeof{\insc}$ 
\noindent $\fzacc{\ectx}{C}$ gathers all the incentives and deposits of the
cycle $C$, mapping each delegate to its amount of frozen Tez.\\*
$\iget{\fzacc{\ectx}{C}}{\ddel}{rwd}$ is the cumulated reward of $\ddel$
in $C$ updated according to the punishement, if any, until $\ectx$.\\*
$\iget{\fzacc{\ectx}{C}}{\ddel}{fees}$ is the cumulated fees of $\ddel$
in $C$ updated according to the punishement, if any, until $\ectx$.\\*
$\iget{\fzacc{\ectx}{C}}{\ddel}{dep}$ is the cumulated deposits of $\ddel$
in $C$ updated according to the punishement, if any, until $\ectx$.

\paragraph{Cumulative Tez creation and burning by cycle.}
\noindent Frozen account of cycle $C$ will be released in the end of evaluation context
of the last block of $C-1 + \pcycle$. $\ctezc{\ectx}{C}$ designates
the created Tez in $C$ by rewards that still available in $\ectx$ and
$\btezc{\ectx}{C}$ designates the burnt Tez in $C$ by punishement that
has been accumulated since $C$ and fees burning for smart contract storage.

\paragraph{Safety freezing period.}
\noindent $\ectx$ preserved the frozen account from $\pastpc{\ectx}$ to
$\pdcycle{\ectx}$: $\fzacc{\ectx}{\cc{i}}.$ At the end of a block
application with evaluation context $\ectx$, the incentives in
$\inss{\ectx}$ are added in $\fzacc{\ectx}{\cc{\ectx}}$.
 If $\cb{\ectx}$ is the last block of $\cc{\ectx}$,
then the frozen account of $\pastpc{\ectx}$ is released. Hence, in the
next block, the first block of $\scycle{\ectx}$, the delegates are credited with
their unfrozen incentives. Similarly, $\ctez{\ectx}$ and resp. $\btez{\ectx}$ increase
$\ctezc{\ectx}{\cc{\ectx}}$ and resp. $\btezc{\ectx}{\cc{\ectx}}$.\\*

\begin{sem}[\tsem{Freezing}]\label{sem:freeze} \ \\
\Fun{Freezing}{\ccst{freeze}: \typeof{\ectx} \rightarrow \typeof{\ectx}} \\*
\Descr{\fz{\prectx}} lifts the current block incentives and token 
to mint and burn to the frozen accounts and returns $\psectx$.\\*
\Post~
 \begin{itemize}
  \item $\fup{\fzaccc{\piectx{1}}}{\cc{\prectx}}{\fzaccc{\prectx}}{\cc{\prectx}}{\inss{\prectx}}$, 
  \item $\fup{\ctez{\piectx{2}}}{\cc{\prectx}}{\ctez{\piectx{1}}}{\cc{\prectx}}{\ctez{\prectx}}$,
  \item $\fup{\btez{\psectx}}{\cc{\prectx}}{\btez{\piectx{2}}}{\cc{\prectx}}{\btez{\prectx}}$.
 \end{itemize}
\end{sem}  \bigskip


\begin{sem}[\tsem{Unfreezing}]\label{sem:unfreeze} \ \\
\Fun{Unfreezing}{\ccst{unfreeze}: \typeof{\ectx} \rightarrow \typeof{\ectx}} \\*
\Descr{\ufz{\prectx}} unfreezes $\fzacc{\prectx}{\pastpc{\prectx}}$ and returns $\psectx$. \\*
\Pre~$\cb{\prectx} \mod \cyclep.$ \\*
\Post~$\forall \ddel, \fup{\wallets{\psectx}}{\wallets{\prectx}}{\addrv{\ddel}}{\wcred{\addrv{\ddel}}{am}}$ where 
    $am = \iget{\fzacc{\prectx}{\pastpc{\prectx}}}{\ddel}{rwd} + 
          \iget{\fzacc{\prectx}{\pastpc{\prectx}}}{\ddel}{fees} + 
          \iget{\fzacc{\prectx}{\pastpc{\prectx}}}{\ddel}{dep}.$
\end{sem}  \bigskip
  
\paragraph{Punishment.}

\begin{sem}[\tsem{Penalizing}]\label{sem:penalty} \ \\*
\Fun{Penalizing}{\ccst{penalize}: \typeof{\ectx} \rightarrow \typeof{\addr} \rightarrow \typeof{\ectx}} \\*
\Descr{\pen{\prectx}{unr}} where
$unr=\{\# n, \ddel, r, f, \bot \}$ returns an updated evaluation context $\psectx$.
A delegate can be penalized 
for having not revealed a seed nonce that
it has committed. In that case the faulty delegate lost its rewards
and fees accumulated in the baked block for which it commits a seed
nonce hash and does not revealed it. \\*
\Post
 \begin{itemize}
   \item $\fup{\fzacc{\piectx{1}}{c}}{\fzacc{\prectx}{c}}{fees({\ddel})}{\iget{\fzacc{\prectx}{\pdcycle{\prectx}}}{\ddel}{fees} - f},$
   \item $\fup{\fzacc{\piectx{2}}{c}}{\fzacc{\piectx{1}}{c}} {rwd({\ddel})}{\iget{\fzacc{\prectx}{\pdcycle{\prectx}}}{\ddel}{rwd} - r},$
  \item $\runass{\piectx{2}}{\ddel}{f}$
\end{itemize}
\end{sem}  \bigskip
 
\subsection{Temporal Context} \label{ss:temp}
\input{Pcarthage/temporal}

\subsection{Endorsement collector} \label{ss:rights}
 $\predendos{\ectx}$ of type 
 $\typeof{\ccst{\CE ndos}}= \typeof{\addr} \rightarrow \typeof{\ccst{e}}$ maps a
 deleguate address to an endorsement right. 

\begin{sem}[Initializing an endorsement collector] \label{sem:endoinit} \ \\
\Fun{Intiating}{\ccst{init}:\typeof{\ectx} \rightarrow \vec{(\typeof{\addr} * \CN)} \rightarrow \typeof{\ectx}}. \\*
\Descr{\einit{\predendos{\ectx}}{l}=}$ \initendo{M}{l}$.\\*
\Pre~$ \sum_{i} n_i = \rendos \forall \addrv{i}, (\addrv{i},n_i) \in l \rightarrow \kcontract{\addrv{i}}=\ccst{\kappa d}$. \\* %todo add a check on total number of slots   
\end{sem} 

\begin{sem}[Recording an endorsement] \label{sem:endorec} \ \\
\Fun{Recording}{\ccst{add}:\typeof{\ectx} \rightarrow \typeof{\addr} \rightarrow \typeof{\ectx}} \\*
\Descr {\erec{\prectx}{\addr}} $= 
\fup{\predendos{\prectx}}{\predendos{\psectx}}{\addr}{\markendo{\addr}{\predendos{\prectx}}}.$ \\*
\end{sem}

\begin{sem}[Collecting endorsements] \label{sem:endocoll} \ \\
\Fun{Collecting}{\ccst{slots}:\typeof{\ectx} \rightarrow \CN} \\*
\Descr {\collectendo{\ectx}}$= \collectendo{\predendos{\ectx}}$. \\*
\Post~$ \collectendo{\predendos{\ectx}} \leq \rendos $
\end{sem}


\subsection{Evaluation Context Validity} \label{ss:valcont}
\input{Pcarthage/context_prop}
