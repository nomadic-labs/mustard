
%% The validation mechanism, in the protocol part deals with the well
%% fondness of blocks, the success of the block or operation applications
%% and the agreement on new heads. The role of the validation is to
%% ensure that in a valid environment, an operation, a block or a new
%% head application is valid and returns a valid environment. Mainly, we
%% denote:

%% \begin{itemize}
%% \item $valid~\mathbb{V}$ a valid validation state $\mathbb{V}$,
%% \item $valid\_env~\mathbb{C}$ a valid evaluation context $\mathbb{C}$ and
%% \item $valid\_ctxt~\mathbb{S}$ a valid shell context.
%% \end{itemize}

%% The statement: $Val~\Gamma_{in} \land \mathbb{I} \Rightarrow Val~(apply~x)~\Gamma_{out}.$ \\*


%% Means that in $\Gamma_{in}$, a set of valid data from the shell to the
%% protocol, the application of $x$ returns a valid $\Gamma_{out}$, a set
%% of wrapped data from the protocol to the shell if I hold. $I$
%% designates the invariants and depends on the specification of the
%% protocol sub-mechanisms that are described in the next sections.

%% A block validation is composed of 3 validation steps: (i) its {\it
%%   pre-state} evaluation context, (ii) its operations and (iii) its
%% {\it post-state} evaluation context.

%% A block validation can has been required in different situations. Each
%% situation is characterized by a validation mode and different pre and
%% post state evaluation contexts. The validation of a received or
%% existing block is performed by {\it application} while the validation
%% of a new build block is performed by {\it construction}. Partial and
%% full variant exist for both case.
%% \begin{itemize}
%%  \item A {\it partial application} is used for the multi-pass validation.
%%  \item An {\it application} is used for a block validation.
%%  \item A {\it construction} is used for a pre-validation.
%%  \item A {\it partial construction} is used for pre-validation without
%%    validation information about the head of the blockchain. Partial
%%    construction is not part of the interface of the protocol, but it
%%    is characterized by validate state.
%% \end{itemize}

%% An evaluation context is then wrapped in a validation state. Each {\it
%%   apply} functions inputs a predecessor blockchain that is abstracted
%%  by the predecessor block $pred$ and its the storage context $\mathbb{S}_{pred}$,
%%   $\tau_{pred}$ the timestamp of its baking and $sc_{pred}$ its fitness
%% Given by reading in the shell storage, this storage context is assumed
%% to be valid: $valid\_ctxt~\mathbb{S}_{pred}$. Anyway, we requires that
%% it is the case, that should be ensured by the formal wrapper between
%% the shell and the protocol.

%% In case of application of an existing block, its header $bh$ is also
%% an input. A block is abstracted by its header. This header leads to
%% its content and metadata as described in~\ref{ingr}. In case of a
%% construction, the block does not exist yet. Instead of inputting a
%% block header $bh$, it inputs the predecessor one $pred$, its height
%% $l_{pred}$ and a timestamp $\tau$, which mocks the time of baking of
%% the mocking block.

%% The application can be partial by considering an ancestor instead of
%% the predecessor. In that case the ancestor has to be younger than the
%% last allowed fork. This $apply$ function is used for multi-pass.

%% A construction is partial without additional parameter. Adding as
%% input a {\tt block\_header\_metadata} that is a header with specific data for
%% the protocol $pbh$ performs a full construction.

%% As output, a construction or an application, partial or full, noted
%% $apply$ returns a validation state that fits to $apply$ and:


%% \begin{center}
%% \begin{tabular}{|l|l|l|l|}
%% \hline
%% $apply$ & validation kind & inputs & outputs \\
%% \hline
%% {\it application} & block & $\mathbb{S}_{pred}, \tau_{pred},sc_{pred},bh_{pred},bh$ & $\{({\tt app }~bh_{pred}~bk~\delay);\mathbb{C}_{out};0\}$ \\
%% \hline
%% {\it partial application} & multipass & ${\bf \mathbb{S}_{anc}}, \tau_{pred},sc_{pred},bh$ & $\{({\tt papp }~bh~bk~\delay);\mathbb{C}_{out};0\}$ \\
%% \hline
%% {\it partial construction} & prevalidation & $\mathbb{S}_{pred}, \tau_{pred},sc_{pred}, pred, l_{pred}, \tau$ &
%% $\{({\tt pconst}~pred);\mathbb{C}_{out};0 \}$ \\
%% \hline
%% {\it construction} & prevalidation & $\mathbb{S}_{pred}, \tau_{pred},sc_{pred}, pred, l_{pred}, \tau, pbh$ & $\{({\tt const }~pred~bk~pbh,\delay);\mathbb{C}_{out};0\}$ \\
%% \hline
%% \end{tabular}
%% \end{center}

\subsubsection{Pre-state Evaluation Context Validation} \label{val_in}

In any case of $apply$, the first step is building an initial
evaluation context $\mathbb{C}_{init}$ such as through {\tt prepare}
from the inputs:
\begin{itemize}
 \item $\mathbb{C}_{init}$ abstracts $\mathbb{S}_{pred}$ (or
   $\mathbb{S}_{anc}$ in case of partial application) that has been
   initial,
 \item $\mathbb{C}_{init}.height$ is $bh.height$ (or $l_{pred}+1$ in case of construction) that has been
   a height of an initial cycle, $\exists cycle(bh.height)$ (resp $\exists cycle(l_{pred}+1)$,
 \item  $\mathbb{C}_{init}.\tau_{pred}$ is $\tau_{pred}$,
 \item  $\mathbb{C}_{init}.\tau$ is $bh.\tau$ (or $\tau$ in case of construction),
 \item  $\mathbb{C}_{init}.fitness$ is $sc_{pred}$,
 \item other values in $\mathbb{C}_{init}$ are set to initial default values.
\end{itemize}

\begin{theorem} [{\bf Pre-state Validation}]
$$valid\_ctxt~\mathbb{S} \land in\_exists\_cycle~bh.height \Rightarrow Val~({\tt prepare}~\mathbb{S}_{pred}, bh, \tau_{pred}, sc_{pred})~\mathbb{C}_{init}.$$
That gives us $valid\_env~\mathbb{C}_{init}$ which is the evaluation
context after $pred$ application.
\end{theorem}


\paragraph{Initial evaluation context of an existing block.}
 {\it begin application} constructs a
valid evaluation context $\mathbb{C}_{out}$ from $\mathbb{C}_{init}$, $bh$ and $\tau_{pred}$, such as those conditions noted {\it valid application}:
\begin{itemize}

 \item $\mathbb{C}_{out}.priority$ is $bh.priority$,

 \item $bh.pow$ is valid regarding the constant {\tt pow\_threshold} in
 $\mathbb{P}$,

 \item the ancestor or predecessor is not older than the max gap:
 $0 < bh.fitness - \mathbb{C}_{init}.fitness \leq \mathbb{P}.max\_gap$,

\item the signature of $bh$ fits with the excepted baker for $bh$ priority:
      $bh.sign = bk$ and the minimum block delay has been respected
 $\mathbb{C}_{init}.\tau -\tau_{pred} \geq \mathbb{P}.block\_delay(bh.priority)$
  and returns the effective block delay $\delay$ with
 $(\mathbb{C}_{init})_{rolls}.check\_priority~bh~\tau_{pred}=(bk,\delay)$,

 \item If a nonce has to be committed at this height, $bh$ must contain
 this revelation: $bh.nonce = |n|$ if
 $\mathbb{C}_{init}.height.has\_commitment$. If no commitment is
 expected for this height then $bh$ must not contain any revelation:
 $bh.nonce = \bot$, otherwise $~\mathbb{C}_{init}.height.has\_commitment$

\item The fitness is increased: $\mathbb{C}_{out}.fitness = \mathbb{C}_{init}.fitness + 1$,

\item  The list of endorsers for the current height of $\mathbb{C}_{init}$, e.g. the predecessor
       height is charged as the {\tt init endorsements}:
        $\mathbb{C}_{out}.init\_endorsements \leftarrow \mathbb{C}_{rolls}.endorsers(\mathbb{C}_{init}.height - 1)$.
\end{itemize}


\begin{theorem}[{\bf Begin Application Validity}]
{\it begin application} returns $\mathbb{C}_{out},bk,\delay$ such as $\mathbb{C}_{out}$ is
 a valid environment $valid\_env~\mathbb{C}_{out}$.
\vspace{-0.1cm}
 $$valid\_env~\mathbb{C}_{init} \land valid~application~\mathbb{C}_{init}~bh~\tau_{pred} \Rightarrow Val (application~\mathbb{C}_{init}~bh~\tau_{pred})~(\mathbb{C}_{out}, \delay).$$
\end{theorem}

\begin{theorem}[{\bf Begin Full Application Validity}]
 {\it Application} returns
$\mathbb{V}_{out}:= \{({\tt app}~bh~bk~\delay);\mathbb{C}_{out};0\}$ that is
a valid state, with  $\mathbb{C}_{rolls}.check\_priority~bh~\tau_{pred}=(bk,\delay)$:
\vspace{-0.1cm}
$$valid\_context~\mathbb{V} \land   \exists cycle(bh.height) \land  pow\_valid~bh.pow  \land $$
\vspace{-0.5cm}
$$  0 < bh.fitness - sc_{pred} \leq \mathbb{P}.max\_gap \land bh.sign = bk \land bh.\tau -\tau_{pred} \geq \delay$$
\vspace{-0.5cm}
 $$  \Rightarrow Val~(application~\mathbb{S}_{pred}~\tau_{pred}~sc_{pred}~bh)~\{({\tt appl}~bh~bk~\delay);\mathbb{C}_{out};0\}.$$
\end{theorem}

\medskip

\begin{theorem}[{\bf Begin Partial Application Validity}]
 {\it Partial application} returns
$\mathbb{V}_{out}:= \{({\tt papp}~bh~bk~\delay);\mathbb{C}_{out};0\}$ that is
a valid state, with  $\mathbb{C}_{out})_{rolls}.check\_priority~bh~\tau_{pred}=(bk,\delay)$:
\vspace{-0.1cm}
$$valid\_context~\mathbb{V} \land  \exists cycle(bh.height) \land  pow\_valid~bh.pow  \land $$
\vspace{-0.5cm}
$$  0 < bh.fitness - sc_{pred} \leq \mathbb{P}.max\_gap \land  bh.sign = bk \land bh.\tau -\tau_{pred} \geq \delay $$
\vspace{-0.5cm}
 $$ \Rightarrow Val~(partial~application~\mathbb{S}_{anc}~\tau_{pred}~sc_{pred}~bh)~\{({\tt pappl}~bh~bk~\delay);\mathbb{C}_{out};0\}.$$
\end{theorem}


\paragraph{Initial evaluation context of a mocking block}

{\it begin partial construction } produces $\mathbb{C}_{out}$ such as
it is $\mathbb{C}_{init}$ with $\mathbb{C}_{out}.init\_endorsements= \mathbb{C}_{rolls}.endorsers(\mathbb{C}_{init}.height - 1)$.


\begin{theorem}[{\bf Begin Partial Construction Validity}]
{\it partial construction} returns $\mathbb{V}_{out}:=\{({\tt pconst}~pred);\mathbb{C}_{out};0\}.$
that is a valid state, with $\mathbb{C}_{baking}.\check\_priority~pbh.height~\tau_{pred}=(bk,\delay)$:
 $$ valid\_env~\mathbb{C}_{init} \land \mathbb{C}_{out}.init\_endorsements = \mathbb{C}_{rolls}.endorsers(\mathbb{C}_{init}.height - 1)  \Rightarrow $$
\vspace{-0.6cm}
$$Val (partial~const~\mathbb{S}_{pred}~\tau_{pred}~sc_{pred}~pred~l_{pred})~\{({\tt pconst}~pred);\mathbb{C}_{out};0\}.$$
\end{theorem}

\medskip

{\it begin full construction}, in addition, inputs $\tau_{pred}$ and $pbh$.
It returns $\mathbb{C}_{out}, pbh, bk$ and $\delay$ and drives the following:
\begin{itemize}
 \item $\mathbb{C}_{out}.priority$ is $pbh.priority$,

 \item the block delay has been respected
 $\mathbb{C}_{init}.\tau -\tau_{pred} \geq \delay$ with
 $\mathbb{C}_{baking}.check\_priority~pbh.height~\tau_{pred}=(bk,\delay)$,

 \item The fitness is increased: $\mathbb{C}_{out}.fitness = \mathbb{C}_{init}.fitness + 1$,

 \item  The list of endorsers for the current height of $\mathbb{C}_{init}$, e.g. the predecessor
       height is charged as the init endorsements:
        $\mathbb{C}_{out}.init\_endorsements~\leftarrow \mathbb{C}_{rolls}.endorsers\mathbb{C}_{init}.height - 1)$.
\end{itemize}

\medskip

\begin{theorem}[{\bf Begin Full Construction Validity}]
{\it begin full construction} returns  $\mathbb{C}_{out}, pbh, bk,\delay$
 with $\mathbb{C}_{baking}.check\_priority~pbh.height~\tau_{pred}=(bk,\delay)$ such as:
 $$ valid\_env~\mathbb{C}_{init} \land  \mathbb{C}_{init}.\tau -\tau_{pred} \geq \delay \land \mathbb{C}_{out}.fitness = \mathbb{C}_{init}.fitness + 1 \land$$
\vspace{-0.5cm}
$$ \mathbb{C}_{out}.init\_endorsements = (\mathbb{C}_{rolls}.endorsers(\mathbb{C}_{init}.height -1)) \Rightarrow valid\_env~\mathbb{C}_{init}.$$
\end{theorem}
Notice that if the if a $pbh$ its means that it has been produced by the protocol.

\medskip

\begin{theorem}[{\bf Begin Construction Validity}]
{\it full construction} returns\\*
 $\mathbb{V}_{out}:=\{({\tt const }~pred~bk~pbh,\delay);\mathbb{C}_{out};0\}$ with $\mathbb{C}_{rolls}.check\_priority~pbh~\tau_{pred}=(bk,\delay)$ such as:
$$ valid\_ctxt~\mathbb{S}_{pred} \land in\_exists\_cycle~pbh.height \land  \mathbb{C}_{init}.\tau -\tau_{pred} \geq \delay \land $$
\vspace{-0.5cm}
$$\mathbb{C}_{out}.fitness = \mathbb{C}_{init}.fitness + 1 \land \mathbb{C}_{out}.init\_endorsements=\mathbb{C}_{rolls}.endorsers(\mathbb{C}_{init}.height - 1)) \Rightarrow$$
\vspace{-0.5cm}
$$ Val (construction~\mathbb{S}_{pred}~\tau_{pred}~sc_{pred}~pred~l_{pred}~\tau, pbh)~\{({\tt const }~pred~bk~pbh,\delay);\mathbb{C}_{out};0\}.$$
\end{theorem}

\subsubsection{Operation Applications} \label{val_op}

{\it apply operation} takes a validation state $\mathbb{V}_{in}$ and
an operation $op$ as input and returns a validation state
$\mathbb{V}_{out}$ and an operation receipt $rc$ as outputs.

The specific case: $\mathbb{V}_{in}.mode$ is a partial application,
 with no operation of passes $0$ (aka. manager operations) then the
 operation counter of $\mathbb{V}_{in}$ is incremented becoming
 $\mathbb{V}_{out}$ and the receipt {\tt No\_operation\_metadata} is
 returned. This case seems to be a work-around or state the end of
a multi-pass validation on the operation of a block.

Otherwise, the predecessor $pred$ and the baker $bk$ is extracted from
$\mathbb{V}_{in}.mode$, when $\mathbb{V}_{in}.mode$ is a partial
construction, a dummy baker $0$ takes the place of $bk$.

{\it eval operation} (denotes the apply operation function in the
apply file) that we simply denote $eval$, takes as input $\mathbb{V}_{in}.ctxt$,
$pred$,$bk$,$(hash~op)$ and $op$. It returns
$\mathbb{C}_{out}$ the evaluation context after applying $op$ and
$(rc~op)$.

Then the result of {\it eval} is wrapped in $\mathbb{V}_{out}, (rc~op)$ with
$\mathbb{V}_{out}:=\mathbb{V}_{in} \leftarrow \{ ctxt \leftarrow \mathbb{C}_{out}; op\_count \leftarrow \mathbb{V}_{in}.op\_count =1\}$.

\begin{theorem}[{\bf Eval Operation Validity}]
$$Val~\mathbb{V}_{out}~\Rightarrow~ Val~(apply~operation~op)~(\mathbb{V}_{out},{\tt operation\_metadata}~rc~op).$$
That is fundamentally depends on:
$$ Val~\mathbb{V}~\Rightarrow~valid\_eval~(eval~\mathbb{V}_{in}.ctxt~pred~bk~(hash~op)~op)~ (\mathbb{C}_{out},(rc~op)). $$
\end{theorem}

As $\mathbb{V}$ comes from a earlier computation of the protocol, we
have $Val~\mathbb{V}$. Consequently, $\mathbb{V}_{in}.ctxt$, noted
$\mathbb{C}_{in}$ is also a valid evaluation context.

\begin{theorem}[{\bf Apply Operation Validity}]
 $valid\_eval$ is the validation of the application of an
operation $op$ in a valid evaluation context $\mathbb{C}_{in}$,
$pred$ and $bk$ that produces the valid evaluation context
$\mathbb{C}_{out}$ and the receipt $(rc~op)$ after the application of
$op$ in $\mathbb{C}_{in}$ under the invariant $\mathbb{I}_{op}$.
$$ valid\_env~\mathbb{C}_{in} \land \mathbb{I}_{op} \Rightarrow Val~(eval~\mathbb{C}_{in}~pred~bk~op) \mathbb{C}_{out},(rc~op).$$
\end{theorem}

The definitions of $valid\_eval$ and $eval$ are provided along with
the specification of the protocol in sections~\ref{accman}
and~\ref{consensus}.


\subsubsection{Applied Block Validation} \label{val_out}

The {\tt finalize block} function of the protocol validates a validation
state $\mathbb{V}$ and returns a validation result
$\mathbb{R}$ and a {\tt block\_header\_metadata} $pbh$.
This function is the last step of a block validation. It would determine
if a block is in the validation process, has been validated
or is a new head. That status depends on the mode in $\mathbb{V}$.

\begin{theorem}[{\bf Finalize Block Validity}]
$$ valid~\mathbb{V} \Rightarrow Val (final~application)~\mathbb{R}, pbh.$$
\end{theorem}


\paragraph{Mocking block application validation.}
A prevalidation that is characterized by $\mathbb{V}.mode= {\tt pconst}$
 only freezes the deposits of delegates computed in
$\mathbb{V}.ctxt$ in an output context $\mathbb{C}$. The
specification of deposits freezing is described in the
section~\ref{accman}. $\mathbb{C}$ is then injected into a
validation result through the finalize function producing:
$\mathbb{R}$ and a mocking {\tt block\_header\_metadata} $pbh$
with a mocking baker $0$, none deactivation of delegate, none balance
updates none gas consumption and none nonce hash but with the current
height.

$valid\_result~\mathbb{R}$ if $valid\_env~\mathbb{C}$. As
$\mathbb{V}$ comes from a previous validation of the protocol,
$valid~\mathbb{V}$, consequently $valid\_env~\mathbb{V}.ctxt$. The
validity of $\mathbb{C}$ depends on the correctness of the
freezing of deposits that is described along with its specification
in~\ref{accman} and depends on the validity of $\mathbb{C}_{frozen}$.
We denote $valid\_frozen~\mathbb{C}_{frozen}$ the validity of $\mathbb{C}_{frozen}$
establishing the correctness of the frozen account computation.

\begin{theorem}[{\bf Finalize Prevalidation}]
The validation of an applied mocking block depends on the validation of its
frozen account.
$$ valid~\mathbb{V} \land \mathbb{V}.mode = {\tt pconst}~\land
   valid\_frozen~\mathbb{C}_{frozen}
\Rightarrow Val (final~application)~\mathbb{R},pbh.$$
\end{theorem}


\paragraph{Validation of an partially applied block.}
The validation of an partial applied block that is characterized by
$\mathbb{V}_{in}.mode= {\tt papp}$ only validates that the block has
reached enough endorsement for declared its predecessor has been a new
valid head. The specification of this computation is presented in the
section~\ref{newhead} and depends on the validity of
$\mathbb{V}_{in}.ctxt_{baking}$. $valid\_append~\mathbb{V}$ is defined
in~\ref{newhead} and states that $\mathbb{V}.bh$ with an effective
block delay $\delay$ reaches the new head conditions is valid in
$\mathbb{V}.ctxt$.

\begin{theorem}[{\bf Finalize Multipass}]
The validation of a partially applied block depends on the new head conditions
that states that the predecessor is a new head.
$$ valid~\mathbb{V} \land \mathbb{V}.mode = {\tt papp}~\land valid\_append~\mathbb{V} \Rightarrow Val (final~application)~\mathbb{R},pbh.$$
\end{theorem}

\paragraph{Validation of an applied existing or fully mocking block.}
The validation of an applied block that is characterized by
$\mathbb{V}.mode= {\tt papp}| {\tt const}$. It produces a wrapped
validation result $\mathbb{R}$ and a related to its $pbh$ from
the output evaluation context and the receipt coming from the
finalize application call with as inputs $\mathbb{V}.ctxt$,
$\mathbb{V}.mode.bh$, $\mathbb{V}.mode.baker$ and
$\mathbb{V}.mode.\delay$, that we denote $\mathbb{C}$, $bh$, $bk$
and $\delay$ and returning $\mathbb{C}_{out}$ and a receipt $rc$ that
is wrapped in $\mathbb{R}$ and $pbh$. First, its validates that
the predecessor block is a valid new head that is described formally
in~\ref{newhead}. Then its computes a valid output evaluation context
through the effects on the context described in~\ref{newhdeffects}.
The validity of this output context remains on the correctness of the
account and organic managements described in sections~\ref{accman}. We
denotes $valid\_new\_head\_effects~\mathbb{C}_{in}~\mathbb{C}_{out}$,
the property that states that if $valid\_env{C}_{in}$ then
$\mathbb{C}_{out}$ the output evaluation context after applying the
effects of a new head appending is a valid evaluation context.

\begin{theorem}[{\bf New Head Validation}]
The validation of an applied block depends on the validation of its
frozen account.
$$ valid~\mathbb{V} \land \mathbb{V}.mode = {\tt const} | {\tt app}~\land valid\_append~\mathbb{V} \land valid\_new\_head\_effects~\mathbb{C}_{in}~\mathbb{C}_{out}$$
$$ \Rightarrow  Val (final~application)~\mathbb{R},pbh.$$
\end{theorem}
