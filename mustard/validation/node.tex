{
\subsection{Node validation model.}
   We define a simplified node model dedicated to the validation sub-system. This model is quite naive and reduces the node to its part involved in the validation sub-system.

   A node accesses to a pool of peers $\peers{\nd}$, e.g. other nodes in the Tezos network. We only mention it for interacting with the pipeline validator.

   A node $\nd$ is instantiated by an economic protocol $\ssp{\nd}$. 
   In this first specification, we avoid protocol switching, however,
   we specify the parametricity of a node by an economic protocol.

   A node $\nd$ manages a blocktree $\bt{\nd}$ from the node storage.
   In the node storage, a block $\bb$ appending is charaterized by its
   shell context $\ctx{\bb}$. Every stored shell context $\sctx$ of
   $\bt{\nd}$ block $\bb$ is a valid shell context, denoted
   $\valctx{\sctx}$ and describes latter in this document and
   in~\cite{ecop} section 2. $\bt{\nd}~\bb$ returns $\sctx$ the shell
   context of $\bb$ that has been stored in the storage after its validation
   such as $\valctx{\sctx}$.

   Genesis, $\gen{\nd}$ is the genesis block in the storage hence in 
   $\bt{\nd}$. Its height is $0$ and it does not have any predecessor.
 
   A node features the finalized prefix, $\sync$, head, aka the
   last allowed fork level of $\ssp{\nd}$ in Tezos jargon, denoted
   $\lafl{\nd}$. Every branch of $\bt{\nd}$ has $\sync$ as a prefix.
   Appending a block in $\bt{\nd}$ can only occur on a head chain
   $\ch$ which is after $\lafl{\nd}$. $\lafl{\nd}$ is potentially
   updated by $\ssp{\nd}$ as the result of a block validation.
   

  In this blocktree, the $\nd$ distinguishes a specific chain, its main chain identified by its head, and named the Shell current head in the Tezos jargon, that we denote $\hd{nd}$. The main chain is the one that the $\nd$ estimates as been shared with the network, e.g. the one that reaches the economic protocol consensus that is abstract as the one with the best score at its head. $\nd$ can switch its
  $\hd{\nd}$ according to the score computed by $\ssp{\nd}$ as a result of block validation.

  A node $\nd$ also contains an experency block date for operations
  contained in a block to append $\mttl{\nd}$. $\mttl{\nd}$ can be
  increased according to $\ssp{\nd}$ computation as a result of a block
  validation. $\nd$ ensures that every operations contained in a
  validated block $\bb$ has a time to live lesser that $\mttl{\nd}$. 


\subsection{Node validation correctness.} 
   A $\bb$ is a correct sucessor of a block $\block{b_n}$ in a node $\nd$ denoted
  $\sisucc{\nd}{\bb}{\block{b_n}}$ and ensures that
  $$\bfit{\block{b_n}}
  < \bfit{\bb} \land 
     \tstp{\block{b_n}} < 
     \tstp{\bb} \land 
     \pb{\bb}
  = \block{b_n} \land \bhg{\block{b_n}} = \bhg{\bb}+1 \land \ctxinc{\bt{\nd}~\bb}{\ssp{\nd}}{\bt{\nd}~\block{b_n}}.$$

  A correct sequence of block in precedence order is a sequence of
  blocks $\block{b_1},\ldots,\block{b_k}$, denoted
  $\cseq{\nd}{\block{b_1},\ldots,\block{b_k}}$ such as $\forall i, 2
  \leq i \leq k \rightarrow
  \sisucc{\nd}{\block{b_i}}{\block{b_{i-1}}}.$

  A chain $\gen{\nd},\block{b_1},\ldots, \block{b_{n-2}},\ch$ is
  characterized by its head $\ch$ and identified by $\ch$. A chain
  $\ch$ is correct in a node $\nd$, denoted $\chcor{\nd}{\ch}$ if
  $$ \sisucc{\nd}{\block{b_1}}{\gen{\nd}} \land \cseq{\nd}{\block{b_1},\ldots, \block{b_{n-2}}} \land \sisucc{\nd}{\ch}{\block{b_{n-2}}}.$$
  
  We denote $\pref{\ch}{n}$ the sub-chain of $\ch$ of $n$ blocks from $\gen{\nd}$.


  We define a correct node $\ncor{\nd}$, a node $\nd$ such as: 
  \begin{enumerate}
  \item $\bt{\nd}$ branches are correct chains: $$\forall \ch \in \bt{\nd}, \chcor{\nd}{\ch},$$
  \item $\bt{\nd}$ branches share $\sync$ as a common prefix: 
    $$\forall \ch_1,\ch_2 \in \bt{\nd}, \pref{\ch_1}{\lafl{\nd}}=\pref{\ch_2}{\lafl{\nd}},$$
  \item $\hd{nd}$ is its best chain, the head branch of $\bt{\nd}$ with the higher
      score: $$ \forall \bb \in \bt{\nd},  \bfit{\bb} < \bfit{\hd{\nd}} \lor  \bfit{\bb} = \bfit{\hd{\nd}}  \rightarrow \tstp{\hd{\nd}} <\tstp{\bb}.$$
  \end{enumerate}
 
\subsection{Correct node corollary.}
  A correct node $\nd$, $\cor{\nd}$ ensures the following properties. \\*

\noindent {\it Blocktree application consistency.}
 The blocktree application consistency,denoted by $\cons{\bt{\nd}}$, ensures
 that in each chain $\ch$ of the blocktree $\bt{\nd}$ of a node $\nd$,
 if $\block{b_i}$ and $\block{b_j}$ belongs to the chain $\ch$ and
 $\pb{\block{b_j}}=\block{b_i}$ then
 $\ctxappv{\nd}
  {\ctx{\block{b_i}}}
 {\ctx{\block{b_j}}}$. \\*


\noindent {\it Strict Monotonicity.}
 The blocktree strict monotonicity, denoted by $\mono{\bt{\nd}}$,
 ensures that in each chain $\ch$ of the blocktree $\bt{\nd}$ of the
 node $\nd$, if $\block{b_i}$ and $\block{b_j}$ belongs to the chain
 $\ch$ and $\pb{\block{b_j}}=\block{b_i}$ then 
$ \bfit{\block{b_i}} < \bfit{\block{b_j}} \land \tstp{\block{b_i}} < \tstp{\block{b_j}} 
  \land \pb{\block{b_i}} = \block{b_j} \land \bhg{\block{b_i}} = \bhg{\block{b_j}}+1.$ \\*


Blocktree application consistency and strict monotonicity are
corollary of node correctness:
$$\forall \nd, \ncor{\nd} \rightarrow \cons{\bt{\nd}} \land \mono{\bt{\nd}}.$$

The node correctness and its corollary are the needed invariant of block validation to deal with blockchain consistency in the network.
