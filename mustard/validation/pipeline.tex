
It may happen that a node receives a block from a peer for which the predecessor is unknown (yet). In that case, the node asks to the peer the missing blocks and validate them. This is the purpose of the \emph{pipeline validation}. As input, the pipeline validation takes a \emph{locator} which is a data structure which encodes a \emph{branch} meaning the missing blocks and as output returns a boolean: \emph{true} if all the blocks have been validated, \emph{false} otherwise.

In this presentation, we simplify the pipeline validation as much as possible to keep things simple. This model will be to refined later to get closer to the actual code-base.

In this model, we modelize a branch of length \(n\) as a sequence of block hashes \((h_i)_{0 \leq i \leq n}\). \(h_0\) is called the \emph{forked point}\footnote{Notice that this model is not realistic since a branch can have an arbitrary size a priori.}.

To start a pipeline validation, the fork point needs to be a valid block for the node. In particular it means that there exists an associated context \(C\).

If the fork point is valid, the pipeline validation will request the blocks one by one to validate them. To do so, we make the assumption that the two nodes communicate through a protocol which understand the message: \texttt{Get\_block \(h_i\)}. If a node receives this message and knows the block associated to the hash \(h_i\), it sends another message \texttt{Block \(b\)} with \(hash(b) = h_i\) ; otherwise the node does nothing.

This model differs from the current behavior of the code-base for the following reasons:
\begin{itemize}
\item Requests \texttt{Get\_block} (resp. \texttt{Block}) are split into different messages such has \texttt{Get\_block\_header} and \texttt{Get\_operations} (resp. \texttt{Block\_header} and \texttt{Operations})
\item A node may sent several request \texttt{Get\_block} to different peers if it is validating several branches at the same time. In practice, there is an intermediate layer which memoizes requests that have been sent through the network.
\end{itemize}

Hence given a branch \((h_i)_{0 \leq i \leq n}\), the pipeline validation iteratively for \(0 < j \leq n\) will fetch blocks one by one and validating them. In this model, the block \(h_{j+1}\) cannot be fetched if the block \(h_j\) has not been validated. Because the branch has been sent by a peer, blocks are requested to the very same peer. If any of the process above fails, the whole validation fails. Notice that requesting a block is a pending process and therefore requires a timeout, otherwise the pipeline validation can be stuck.


\paragraph{Validation pipeline model.}
We modelize the pipeline validator as a correct black box functions
from the block validation point of view. A validation node model $\nd$
features with a map of pipeline validator$\pv{\nd}$ such as:$\pvp{\nd}{p}$ where
$p \in \peers{\nd}$. A pipeline validator $\pvp{\nd}{p}$ features a
timeout denoted by $\tout{\pv{\nd}}$. To distinguish the case
validation block inside a pipeline validation from a simple one (e.g.,
when we already knows the chain without recovering), a node, ${\nd}$, 
features a status, $\st{\nd}$ that is
either:
\begin{itemize}
\item $\off$ means that the validation is not part of a pipeline validation, 
\item $\first{ch}{\bb}$ means that the first block between $ch$, the common prefix of the node
      $\nd$ and the to-validate chain $\bb$, has to be validated,

\item $\inter{ch}{\bb}{i}$ means that the $(\bhg{\bb} - i - 1 )th$ block between $ch$ and
 $\bb$ has been validated and the $\bhg{\bb} - i$ has to be validated and $i$ blocks until $\bb$
 has to be valided,
\item $\last{ch}{\bb}$ means that the last block between $ch$ and $\bb$, e.g. $\pb{\bb}$
 has been validated and $\bb$ has to be validated.
\end{itemize}
Once $\bb$, e.g. the received block and head of the branch to validate, 
validation begins, $\st{\nd} \leftarrow \off$.

In this first formalization, only one branch validation
occurs at the same time, no simple validation are performed in the
meantime. In later version, we will deal with a more realistic model.


\noindent {\it Branch recovering.} A block validation requests a recovering, denoted by
$\recovering{\pv{\nd}}{\bb}{\bt{\nd}}{\peers{\nd}}$, to the pipeline
validator map $\pv{\nd}$ for the lacked block and the common prefix between
the blocktree of the node $\bt{\nd}$ and the rest of its connected
peers $\peers{\nd}$.

\noindent When succeed, $\recovering{\pv{\nd}}{\bb}{\bt{\nd}}{\peers{\nd}}$ returns a
pair of the common prefix head $\block{common}$ and the lacked block
lists $\block{lb}$ with
$\block{lb}= \block{lb_0}, \ldots, \block{lb_n}$, such as:
$$(Prec)~ \recovering{\pv{\nd}}{\bb}{\bt{\nd}}{\peers{\nd}} = (\block{common},\block{lb}) \rightarrow \mon{\block{common}}{\block{lb_0}} \land (\forall 0 <i <n, \mon{\block{i-1}}{i}) \land  \mon{\block{lb_{n-1}}}{\bb}.$$

The recovering fails if the pipeline validator does not succeed to identify the lacked blocks.
 or the common prefix before its timeout $\tout{\pv{\nd}}$ expired.


\noindent {\it Branch validation.}

\noindent $\nbchappend{\nd}{\block{common}}{\block{lb}}{\bb}$ returns $\ndv{res}$, validating the lacked sub-branch $\block{lb},\bb$ to $\block{common}$, as follows:

\begin{itemize}
 \item Beginning by the validation of $\block{lb_0}$ appending to $\block{common}$ chain:
 $$\st{\nd} = \first{\block{common}}{\bb} \rightarrow \nbappend{\block{common}}{\nd}{\block{lb_0}} = \ndv{0} \land  \inter{\block{common}}{\bb}{\bhg{\bb} - \bhg{\block{common}}},$$

 \item following by the validation of $\block{lb{n-i}}$ appending to $\block{(n-i)-1}$ chain,where $n = \bhg{\bb} - \bhg{\block{common}} - 2$:
   $$\forall 0 < i -1 < n, \st{\ndv{i-1}} = \inter{\block{common}}{\bb}{i-1} \rightarrow \nbappend{\block{lb_{n-i}}}{\ndv{i - 1}}{\block{lb_{n-(i+1)}}} = \ndv{i} \land \st{\ndv{i}} = \inter{\block{common}}{\bb}{i}.$$

 \item flollowing by the last lacked block appending validation:
 $$ \st{\ndv{n-1}} = \inter{\block{common}}{\bb}{1} \rightarrow \nbappend{\block{lb_{n-1}}}{\ndv{n-1}}{\block{lb_{n}}} = \ndv{n} \land \st{\ndv{n}}=\last{\block{common}}{\bb}, $$ 

 \item ending by the validation of $\bb$ appending to $\block{lb_n}$ chain:
     $$\st{\ndv{n }}= \last{\block{common}}{\bb} \rightarrow \nbappend{\block{lb_n}}{\nd}{\bb} = \ndv{res} \land \st{\ndv{res}} = \off.$$

\end{itemize}

The branch appending validation fails on any of its block appending failure, or does not finish
before $\tout{\pv{\nd}}$.


\paragraph{Branch validation correctness.}
A branch validation initiated by a correct node, $\nd$, with $\ncor{\nd}$, if the prefix for appending $\bb$ has the kind $\selectb{\nd}{\ch}{\bb}=\ukpfx~\ch~\bb$. 
In that  case, $\nd$ requests a recovering and branch validation to $\pv{\nd}$: 
$\recovering{\pv{\nd}}{\bb}{\bt{\nd}}{\peers{\nd}}$.
If the recovering succeed to get the lacked branch from the network: 
$\recovering{\pv{\nd}}{\bb}{\bt{\nd}}{\peers{\nd}} = (\block{common},bl)$. 
The pipeline validator then calls iteratively the block validation as described above
through $\nbchappend{\nd}{\block{common}}{\block{bl}}{\bb}$. If it succeed it returns $\nd{res}$
a correct node, $\ncor{\nd{res}}$ and consequently the blocktree properties are preserved.

$$\ncor{\nd} \land  \selectb{\nd}{\ch}{\bb}=\ukpfx~\ch~\bb \land 
  \recovering{\pv{\nd}}{\bb}{\bt{\nd}}{\peers{\nd}} = (\block{common},\block{bl}) \land $$ $$
 \nbchappend{\nd}{\block{common}}{\block{bl}}{\bb}= \ndv{res} \rightarrow \ncor{\ndv{res}}.$$

The proof remains on the property $(Prec)$ and the correctness of $\nbappend{}{}{}$. 
