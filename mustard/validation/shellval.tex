
\paragraph{Blockchain data structure abstraction.}
Locally, each node maintains its replica of the blockchain data
structure, also called the ledger. We ignore the testchain, as we do
not deal with the self-amendment mechanism, making us consider the
storage as the ledger that associates to each validated block \(\bb\) a
context \(\sctx\). We abstract the context associates to a block
\(\bb\) as the ledger content after application of \(\bb\) as it is adding to
the ledger only after \(\bb\) application and validation.

The main chain on a replica is abstract by a current head, which is a
block \(\bhead\). The main chain can be reverse computed by following the
predecessor blocks from \(\bhead\) to genesis.

The protocol allows certain forks limited in ages through the
parameter \vlaf{} which is the older height on which
forks are authorized, which makes the ledger been a blocktree such as
from genesis to \vlaf{} is the common prefix, which is
a chain and from \vlaf{} the blocktree might contains
several branches. There is always a main chain that is the chain identify
by the current head \(\bhead\) of the replica.
Each of the notions above are more details in the zombie octopus specification~\todo{link}.
There are just remind here to ease the understanding.

A blocktree is then an abstraction that contents a storage
$\sctx$, a head \(\bhead\) and the height that delimited the common
prefix from the allowed forks \vlaf{}. A storage
$\sctx$ maps to each validated block \(\bb\) a storage context
$\ctx{\bb}$. That invariant of the ledger is obtained by
construction as every writing in the ledger is performed after having
applying and validating a block.


Validation in \tezos{} is done with the following modules:
\begin{itemize}
\item In \ocaml{lib_shell}
  \begin{itemize}
  \item \ocaml{chain_validator}
  \item \ocaml{block_validator}
  \item \ocaml{peer_validator}
  \item \ocaml{block_validator_process}
  \end{itemize}
\item In \ocaml{lib_validation}
  \begin{itemize}
  \item \ocaml{block_validation}
  \item \ocaml{external_validation}
  \end{itemize}
\item In \ocaml{bin_validation}
  \begin{itemize}
  \item \ocaml{validator}
  \end{itemize}
\end{itemize}

\warning{The order of dependencies of these modules does not reflect the execution order because of callbacks}
\todo{picture, diagrams}

\paragraph{Singleprocess versus external validation.}
The validation of a block may take a while and therefore the node can
delegate the validation of a block to another processus. This is the
default mode. However, one can keep the sequential mode using the
option \mintinline{bash}{--singleprocess}. The module
\ocaml{block_validator_process} is the one which knows whether we are
in sequential mode or not. Notice, that it has no consequence on block
validation, it drives entry points to the block validation.


\paragraph{Validation of block versus validation of a branch}
When a node is connected to a peer, the entry point to validate new
blocks is the \ocaml{peer_validator} module from \ocaml{lib_shell}.
The \ocaml{peer_validator} module calls directly the
\ocaml{block_validator} module if there is only one block to validate.
Otherwise, it triggers the \ocaml{bootstrap_pipeline}.
The \ocaml{bootstrap_pipeline} will starts from a
\ocaml{block_locator} sent by the remote peer and will start to
download \ocaml{block_headers} and \ocaml{operations} before
validating them. The validation process from the current
\ocaml{bootstrap_pipeline} is exactly the same as validating block one
by one through the \ocaml{block_validator} module.

\subsection{From the shell to the protocol}

The entry point for the validation of a block from the shell is the
function \ocaml{block_validator.validate}. This function takes a
callback \ocaml{?notifty_new_block}. This callback comes from the
\ocaml{chain_validator} module (see
\ocaml{chain_validator.notify_new_block}). This callback will be
called once the block has been validated by the protocol.

It is particularly important to understand that it is only the
\ocaml{fitness} announced which determines whether we start a
\ocaml{bootstrap_pipeline} or a call to
\ocaml{block_validator.validate}. If the peer lies on the
\ocaml{fitness} this can be detected by the protocol.



\paragraph{Block validator}
Because behind the \ocaml{block_validator} we have a
\ocaml{tezos-worker}, this function is really simple and just push a
\ocaml{Request_validation} for the worker. At that point, the shell
needs to ensure one thing: It knows the protocol and it has been
compiled already. This is the purpose of the different calls to
\ocaml{Protocol_validator.prefetch_and_compile_protocols}. This
function will call the \ocaml{Block_validator_process.apply_block}
function but ensures the following invariants before:
\begin{itemize}
\item The block is consistent with the current
  \ocaml{State.save_point} of the shell. This value registered the
  oldest block for which we knows some \ocaml{metadata} useful in
  \ocaml{Rolling} and \ocaml{Full} mode.
\item The block increases the current fitness of the chain
\end{itemize}

If the function \ocaml{block_validator_process.apply_block} executes
successfully, then the block is commited through the
\ocaml{Distributed_db} onto the disk and we call
\ocaml{notify_new_block}. Otherwise, the block is registered as
invalid (again through the \ocaml{Distributed_db}).

Another thing is that we made a subsequent call to
\ocaml{Protocol_validator.prefetch_and_compile_protocols} to trigger
the download of a new protocol (and the compilation) if it is
required.


Besides implementation parameters to handle call back and error
processing, the validation of a block \(\bb\) requires to parametrized
\ocaml{block_validator.validate} by a chain identifier that we
ignore, an hash $\bpred$ that seems to be the hash of the predecessor of
\(\bb\) in the block tree \(\tbt\), \(\bb\) block header \(\bhb\) and \(\bb\) operations
$\oops$. We denote $\ctx{\bpred}$ the context associated to $\bpred$
in the blocktree \(\tbt\). If such $\ctx{\bpred}$ does not exist, then the
validation failed. Its existence states that \(\bpred\) is a valid block.
Concerning $\bpred$ either it is \(\bhead\) or its height $\lvl{\bpred} \geq
\vlaf{}$.

Notice, that the existence of $\ctx{\bpred}$ in the storage
states that $\bpred$ has been validated and $\ctx{\bpred}$ is
the context after $\bpred$ has been applied. This is a properties of the
storage that is obtained by construction: new contexts are \emph{commited} in the
storage only after validation.
\[\exists \ctx{\bb},~\fctxof{\bb} = \fsome{\ctx{\bb}}
\simpl \val{\bb} \land \val{\ctx{\bb}}.\]

\paragraph{Block validator process}

As mentioned before, this module is the one which implements the
switch between the singleprocess mode and the external process mode as
described before. Moreover the call to
\ocaml{Block_validator_process.apply_block} triggered by the
\ocaml{Block_validator} module triggers a consistency check about the
liveness of operations. The \ocaml{Block_validator_process} checks if:
\begin{itemize}
\item there is no duplicate operations, we denote $\fnodup{ops}$,
\item Each operation is based upon a block whose not too far from our
  current head (this distance is determined by
  \ocaml{operations_max_ttl} which defaults value is \(60\)),
  $\faliveops{\ctxmttl{\bb}}{\oops}$.
\end{itemize}



Then, it requests to the shell the context of the previous block. If
everything is ok, the \ocaml{block_validator_process} will eventually
call the \ocaml{Block_validation.apply} function which is responsible
to call the \ocaml{apply} function of the appropriate protocol.


\paragraph{Block Validation of Validation Library}
Specification of the {\tt apply} function in {\tt
  lib\_validation/block\_validator.ml}

parameters:
\begin{itemize}
\item \vmttl: the maximum time to live of an operation (dependent of a block),
\item \(\bhb\): the block header of the block to validate,
\item $\bhpred$: predecessor block header of \(\bhb\),
\item $\ctx{\bpred}$: evaluation context after the application of \(\bpred\),
\item $\oops$: operations contained in \(\bhb\).
\end{itemize}

According to the function called stack until here we have the
following properties, that we denote $\pred{in}{apply}(\bhb, \bhpred, \ctx{\bpred}, \oops)$:
$$\fctxof{\bpred} = \fsome{\ctx{\bpred}} \simpl
\val{\bpred} \land \fnodup{\oops} \land \faliveops{\ctxmttl{\bpred}}{\oops}$$


\subsubsection{Validation Input Requirements}

\paragraph{Announced monotony checking.}
\begin{enumerate}
\item It checks whether the announced height is consistent
\item It checks whether the time-stamp is consistent
\item It checks whether the fitness is consistent
\end{enumerate}


\(\pred{shell}{monotonicity}(\bhpred,\bhb)\):
$$\lvl{\bhb} = \lvl{\bhpred} +1
\land \bhb.proto  \in \ctxprotos{\bpred} \land  \pre{\bhb} = \hpred \land \bhb.\tau > \bpred.\tau \land \bhb.sc > \bpred.sc $$

This monotony check seems to be a requirement for a protocol to be valide \todo{to detail}.

\paragraph{Well-shaped block.}
\begin{enumerate}
 \item It checks whether the number of validation passes is correct. As a
 remainder, a block from the shell is a head and a list of list of
 operations. The \ocaml{Proto.validation_passes} corresponds to the
 maximum size of the external list. It is \emph{necessary} that the
 number of passes in a block is always equal to
 \ocaml{Proto.validation_passes}. Hence a block with no operations is
 actually a list of \ocaml{Proto.validation_passes} empty lists.
 \item It also checks whether operations are valid with respect to the quota of
 the protocol.
 \item Finally, it checks that each operations is in its corresponding pass.
\end{enumerate}
We denote ops\_shape the operation passes that one can found misleading.

$ well-shaped~\bhb$:

$
\begin{array}{lcl}
~~~~ & & \bhb.ops\_shape \land \mathbb{P}.ops\_shape (validation\_passes) \land \forall i, |ops_i| \leq \mathbb{P}.ops\_shape_i.max\_op \\
~~~~ & \land & (\forall j, size\_of~(ops_i)_j \leq \mathbb{P}.ops\_shape_i.max\_size \land  \forall i, \forall j, in\_good\_pass~(ops_i)_j i. \\
\end{array}
$
\\

$good\_pass~op~i$ is defined as follow in the protocol:
\begin{itemize}
\item $good\_pass~Endo~0$
\item $good\_pass~Proposal|Ballot~1$
\item $good\_pass~Seed\_nonce|Evidence|Activation~2$
\item $good\_pass~Manager|Cons~3$.
\end{itemize}

\paragraph{Wrapping the storage context into a shell context.}
The shell context is a wrapping of a storage context that extends
the storage context with with 2 function {\tt set\_protocol} and {\tt fork\_test\_chain}.
As this abstraction does not include the self-amendment mechanism,
the shell and the storage object is the same.


\subsubsection{Validation}

The next step is then to sequentially calls several application
function of the economic protocol.

\paragraph{Allocate the block.}
A block is valid is its appending on its predecessor is valid: its proof-of-work is
valid and its preserved monotony of the chain.

First to prepare and initialize the ledger from the predecessor $\bpred$
storage context $\ctx{\bpred}$ to the allocation of \(\bhb\) on top
of it, the function begin application is called and verify the
monotony of this appending, the proof-of-work of the \(\bhb\) and the
time-stamp consistence between the $\bpred$ and \(\bhb\) baking. If these
properties are verified, then a validation state $\vres{0}= \{{\tt
  app~}\bhb,bk,\delta,\pctx{0},0\}$ where $bk$ is the baker of \(\bhb\),
$\delta$ is the duration between $\bpred$ and \(\bhb\) baking, $0$ is the
counter of applied operation of this validation and $\pctx{0}$ is
an evaluation context, an higher-level abstraction of a storage
context that features ease the reasoning on the protocol mechanism.
We denote $alloc$ the begin application function of the protocol, $\val{pow}$ the validation of a proof-of-work.

$$\vapp~\ctx{\bpred}~\bpred~\bhb~\vres{0}: \vres{0} = alloc~\ctx{\bpred}~\bpred~\bhb \land  \val{pow}~\bhb.pow \land \bhb.sign=bk \land \delta = \bhb.\tau - \bpred.\tau $$$$ ~~~~~~~~~~~~~~~\land 0 < \bhb.sc - \bpred.sc \leq max\_gap \land \val{\vres{0}}.$$

%$\begin{array}{lcl}
%~~~~~~~~& &\vres{0} = alloc~\ctx{\bpred}~\bpred~\bhb \land  \val{pow}%~\bhb.pow \land \bhb.sign=bk \\
%~~~~~~~~&\land & \delta = \bhb.\tau - \bpred.\tau \land  0 < \bhb.sc - %\bpred.sc \leq max\_gap \land \val{\vres{0}}.\\
%\end{array}
%$

Notice that $max\_gap$ is $1$.

\paragraph{Operation applications.}
A block is valid if its application succeed, it means that the
application of the operations, that it contains, succeed.

An operation $op$ application $eval~op$ in a valid validation state
$\valres$ returns $\vres{op},rc~op$ if the application
succeed where $rc~op$ is the receipt of the operation application and
$\vres{op}$ differs from $\valres$ by its operation counter
that has been increased and its evaluation context such as:
$eval~op~\valres.ctxt~=~\vres{op},rc~op$.

 We then can abstract the operation sequence application $evals$ as follows:
$$ \vcont~\vres{0}~\oops~\vres{ops}:~evals~\oops~\pctx0=\vres{ops}, \vec{rc~op_{i}} \land ops = op_0,....op_{n-1} \land \vres{ops}=\{{\tt app~}\bhb,bk,\delta,\pctx{ops},n \}.$$


\paragraph{Appending the block.}
A block is valid if it is appended to a valid blockchain: in addition
of being the current head of the ledger, its predecessor has to reach
in \(\bhb\) operation \(\oops\) enough endorsements $|endos|$ according to
\(\bhb\) priority $\bhb.priority$ and the time between $\bpred$ and \(\bhb\)
baking $\delta$. That law is called minimal allowed endorsement in
Emmy+ and it is wrapped in the predicate $\val{append}~\bpred~\bhb$.
Besides, some effects are performed on the context to reward
incentives and update cycle and height information as well as updating
the score of \(\bhb\) so that it comes the new \(\bhead\) of the $blocktree$,
as $\bpred$ was the \(\bhead\) before \(\bhb\) application. Notice, that the
\(\bhead\) updates takes place latter. We denote this modified evaluation
context $\pctx{out}$ that is wrapped in a storage context $\BS_{out}$
with the following properties:
$$ \vch~\pctx{ops}~\bpred~\bhb~\BS_{out}:~\exists \pctx{out}, wrap~\pctx{out}~\BS_{out} \land \val{nheffects}~\pctx{ops}~\pctx{out} \land \val{\pctx{out}} \land \val{append}~\bpred~\bhb.$$

\paragraph{Composed the steps.}

We define $\vapply$ the validation of a block application and $apply$
the block application function:
$$\vapply~\ctx{\bpred}~\bpred~\bhb~\oops~\BS_{out} :~ \exists \vres0, \vres{\oops}, \vres{0}=  apply~\ctx{\bpred}~\bpred~b \land $$
$$evals~\oops~\pctx0=\vres{ops}, \vec{rc~op_{i}} \land (\exists \pctx{out}, \val{nheffects}~\pctx{ops}~\pctx{out} \land wrap~\pctx{out}~ \BS_{out}) \land$$
$$ \vapp~\ctx{\bpred}~\bpred~\bhb \land \vcont~\vres{0}~\oops~\vres{\oops} \land \vch~\pctx{\oops}~\bpred~\bhb~\BS_{out}.$$

\paragraph{Chain validator}

If a block has been validated successful through the
\ocaml{Block_validation.apply} function, the callback
\ocaml{Chain_validator.notify_new_block} is triggered. This function
triggers the request \ocaml{Validated}. When evaluated, this requests
checks one last time that the fitness is still better (or equal) than
the current fitness. In that case, this block becomes the new head of
the chain. The head is broadcasted to the connected peers (if the node
is bootstrapped) and the prevalidator is updated accordingly.
