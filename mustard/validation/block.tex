
The validation of block $\bb$ appends on $\ch$ in a node $\nd$,
$\nbappend{\bb}{\ch}{\nd}$ returns a node.

\subsection{Branch selection.}
The branch selection $\selectb{\nd}{\ch}{\bb}$ characterizes the
appending that the block validation will produce.

\noindent{\it Check block candidate.}
The branch selection begins by checking if $\bb$ is a correct block candidate:
(i) The appending concerns a block that have not yet been
validated: $\bt{\nd}~\bb=\bot$ and returns $\know~\bb$ otherwise.
(ii) It check that this block appending will ensure a correct growing of
the $\bt{\nd}:~\lafl{\nd} < \bhg{\bb}.$\\*


\noindent{\it Prefix kind.}
Then the branch selection characterizes the appending prefix $\ch$ returning a prefix kind:

\begin{itemize}

\item $\uvpfx$ denotes that $\ch$ is a prefix of the common prefix: $\bhg{\ch} < \lafl{\nd}$.
\item $\kpfx~\ctx{\ch}$ denotes that $\ch$ is not a prefix of the common prefix and that the node already knows $\ch$ and its shell context is $\ctx{\ch}$:
$\lafl{\nd} \leq \bhg{\ch} \land \bt{\ch} = \ctx{\ch}.$
\item $\ukpfx~\ch~\bb$ denotes that $\ch$ is not a prefix of the common prefix and has not yet been
validated by the node:$\lafl{\nd} \leq \bhg{\ch} \land \bt{\ch} = \ctx{\ch}.$

\end{itemize}

$\uvpfx$ aborts the block validation and returns $\nd$.
$\ukpfx~\ch~\bb$ delegates the branch $\ch$ recovery to the pipeline validator.
$\kpfx~\ctx{\ch}$ is the case that triggers a block application.\\*

The following describes the next steps after the prefix selection where the prefix is known:
$\selectb{\nd}{\ch}{\bb}= \kpfx~\ctx{\ch}$. In these paragraphes we refer to definitions
and properties specifies in~\cite{ecop} section 2.4.1. \\*

\subsection{Operation liveness.}
In order to be valid, the block $\bb$ content, $\oops$ should comply with the
the maximum time to live of $\ctx{\ch}$, $\ctx{\ch}.mttl$. This verification
is denoted $\cmttl{\nd}{\ctx{\ch}}{\bb}$. 

\subsection{Strict monotonicity preservation.}
The strict monotony checking $\mon{\ch}{\bb}$ verifies that 
$\bb$ is a potential sucessor of $\ch$, that it has been baked after $\ch$
and that its score is better.
 $$\bfit{\block{\ch}} < \bfit{\bb} \land \tstp{\block{\ch}}< \tstp{\bb} \land \pb{\bb}  = \ch \land \bhg{\bb} = \bhg{\ch}+1.$$

The monotony checking is confirmed after the block application by
 checking that the declared score in $\bb$ and the score computes by
 the block application is the same. 

\subsection {Block shape checking.}
The shape of a block has to comply the economic protocol to be applied.
$\shaped{\nd}{\bb}$ works as defined in~\cite{ecop} the section 2.4.1.
\begin{enumerate}[label=(\roman*)]
\item Operation passes are ordered in pass order, 
%  $\forall k, \forall i~j, i < j \rightarrow \ocomp{\oopse{k}{i}}{\oopse{k}{j}}$,
\item Each operation pass is ordered by operation order,
%  $\forall k k', k < k' \rightarrow (\forall i~j, \ocomp{\oopse{k}{i}}{\oopse{k'}{j}})$,
\item Operation pass contains only operation of this pass,
% $\forall k~i, \oofkind{\oopse{k}{i}}=k$,
\item Each operation size compliance with the pass maximum size,
% $\forall k~i, \osizeof{\oopse{k}{i}} \leq \passsize{k}$,
\item Each pass contains a number of operation that complies with the requested number for this
pass.
%  $\forall k, |\oopsk{k)}| \leq \passelts{k}$.
\end{enumerate}

 
\subsection{Block application}
In the same flavor, the block application is described in~\cite{ecop} section 2.4.1.

We remind the application function, $\app$, specification:
\emph{Suppose $\bb$ obeys to the protocol block shape, $\bpred$ is its predecessor and
the application of $\bb$ as a block appending on $\bpred$ returns $\valres,pbh,\resops$.
$\valres$ is a valid validation result regarding $\pp$ and $\bb$ and
$\resops$ is the list of valid operation results regarding $\ctx{\bpred}$ and $\ctx{\valres}$:
$$\pbshaped{\bb} \land \issucc \land \app~\prefix{\bpred}~\bb= (\valres,\pbh,\resops) \rightarrow \valvres{\valres}{\bpred}{\bb}{\pp} \land \valrcp{\resops}{\ctx{\bpred}}{\ctx{\valres}}.$$}


Hence, $$ \shaped{\nd}{\bb}\land \mon{\ch}{\bb} \land \app_{\ssp{\nd}}~\prefix{\ch}~\bb = (\valres,\pbh,\resops) \rightarrow  \valvres{\valres}{\ch}{\bb}{\ssp{\nd}} \land \valrcp{\resops}{\ctx{\ch}}{\ctx{\valres}}.$$


$\valvres{\valres}{\ch}{\bb}{\ssp{\nd}}$ states that $\valres$ is a valid validation result
and that: 
\begin{enumerate}[label=(\roman*)]
\item  $\bb$ application preserved the  consistency of $\ch$ content and: $\ctxappv{\ssp{\nd}}{\ctx{\ch}}{\ctx{\bb}}$,
\item  $\ch$ is enough popular after $\bb$ application: $\popular{\ctx{\bb}}{\ch}$,
\item  The score of $\bb$: $\fit = \cfit{\ssp{\nd}}{\bb}{\ctx{\bb}} $,
\item  maximum operation time-to-live update: $\maxttl= \bhg{\bb} - \maxttl_{\ssp{\nd}}$, and
\item  finalized chain update: $\sync = \bhg{\bb} - \gap_{\ssp{\nd}}$.
\end{enumerate}

\subsection{Monotonicity confirmation.} 
Once the application succeed, the monotonicity is confirmed by verifying that $\bfit{b} = \fit$.
If the monotony is not confirmed, the validation is rejected.

\subsection{Appending.}
$\btadd{\nd}{\bb}{\ctx{\bb}}$ returns a blocktree $\bt{res}$ such as: 
$$\bt{res}~\bb \leftarrow \ctx{\bb} \land 
  (\forall \block{b'}, \block{b'} \neq \bb \rightarrow 
        \bt{res}~\block{b'} \leftarrow \bt{\nd}~\block{b'})$$


\subsection{Main chain.}
The main chain of $\nd$, e.g. its head $\hd{\nd}$ must be compared with $\bb$,  
(e.g. the chain $\ch,\bb$) if $\fit \leq \bfit{\hd{\nd}}$ then the main chain will
not be updated, otherwise, $\bfit{\hd{\nd}} < \fit$, $\bb$ becomes the main chain
of the resulting node. Consequently to a main chain update, the common prefix and the maximum 
time to live might also be updated.
$\commit{\nd}{\bb}{\valres}$ returns the potentially updated head, common prefix and maximum
 $$\fit \leq \bfit{\hd{\nd}} \rightarrow \commit{\nd}{\bb}{\valres} = (\hd{\nd},\lafl{\nd},\mttl{\nd}) \lor $$ 
$$ \bfit{\hd{\nd}} < \fit \rightarrow \commit{\nd}{\bb}{\valres} = (\bb,\valres.\sync,\maxttl). $$

\subsection{Appending case specification.}

A successful $\bb$ block appending to $\ch$ in the node $\nd$ 
returns\\* 
\noindent $\commit{\btadd{\nd}{\bb}{\ctx{\bb}}}{\bb}{\valres} $ if:
\begin{enumerate}[label=(\roman*)]
 \item $\bb$ is a block candidate: $\bt{\nd}~\bb=\bot \land \bt{\nd}:~\lafl{\nd} < \bhg{\bb},$
 \item $\ch$ is a known appendable chain of $\nd$:  $\selectb{\nd}{\ch}{\bb}= \kpfx~\ctx{\ch}$, 
 \item operation of $\bb$ liveness: $\cmttl{\nd}{\ctx{\ch}}{\bb}$,
 \item $\bb$ is strictly higher than $\ch$:  $\mon{\ch}{\bb}$,
 \item $\bb$ shape complies with $\ssp{\nd}$: $\shaped{\nd}{\bb}$,
 \item The application of $\bb$ to $\ctx{\ch}$ successed:$\app_{\ssp{\nd}}~\prefix{\ch}~\bb = (\valres,\pbh,\resops)$,
% \valvres{\valres}{\ch}{\bb}{\ssp{\nd}} \land \valrcp{\resops}{\ctx{\ch}}{\ctx{\valres}} $,
\end{enumerate}
then: 
 $$\nbappend{\bb}{\ch}{\nd} = \commit{\btadd{\nd}{\bb}{\ctx{\bb}}}{\bb}{\valres}.$$


\subsection{Returning a correct node.}
 A valid block appending iof a block $\bb$ to the chain $\ch$ in a
node $\nd$ returns a correct node if $\nd$ is a correct node:
 $$\forall \nd,\bb,\nd, \nbappend{\bb}{\ch}{\nd} \neq \bot \rightarrow \ncor{\nd}.$$ \\*
