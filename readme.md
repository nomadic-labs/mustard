The Mustard repository contains the mustard paper of The Tezos
Blockchain Protocol.  It gives a formal specification and its
high-level properties.

`mustard/mustard/` contains the latex source in a sole file so far: `mustard.tex`

**The Mustard document is a work in progress**.

## Compiling the document(s)

In short, type
```
make all-publish
```

We use some automation to compile `LaTeX` files from the different
directories, providing `draft` and `publish` versions of the
documents. The `draft` versions enable clearly marked, author and
reviewer contents.

The default target is `all-draft`, which generates all draft
versions. For each document/folder, the `x.draft` target generates the
draft version, whereas the `x.pdf` target generates its public
version.
